module.exports = {
    entry : "./src/index.js",
    output : {
        path          :  __dirname,
        filename      : "dist.js",
        library       : "LonnyReduxNode",
        libraryTarget : "umd",
        globalObject  : "this"
    },
    module :  { 
        rules : [{
            test : /\.js$/,
            use  : [ "babel-loader" ],
        }]
    }
};
