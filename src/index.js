const ACTION_TYPE = "BOKO_ACTION"

const lens = (nodeDef) => (state) => {
    const result = state[nodeDef.namespace]
    return result === undefined ? nodeDef.initialState : result
}

const get = (nodeDef) => (store) => {
    return lens(nodeDef)(store.getState())
}

const set = (nodeDef) => (data) => (store) => {
    store.dispatch({
        type: ACTION_TYPE,
        namespace: nodeDef.namespace,
        data: data
    })
}

export const reducer = (state = {}, action) => {
    if( action.type === ACTION_TYPE)
        return {
            ... state,
            [ action.namespace ] : action.data
        }
    return state
}

export const middleware = (store) => (next) => (action) => {
    if(typeof action === "function")
        return action(store)
    return next( action )
}

let namespaceCounter = 0
export default (initialState = {}) => {
    const namespace = `node_${namespaceCounter++}`
    const nodeDef = {namespace, initialState}
    return {
        lens: lens(nodeDef),
        get: get(nodeDef),
        set: set(nodeDef)
    }
}